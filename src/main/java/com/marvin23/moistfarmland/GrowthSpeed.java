package com.marvin23.moistfarmland;

import java.util.Arrays;

public enum GrowthSpeed {

    STOPPED(0, "stopped"),
    VERY_SLOW(40, "very slow"),
    SLOW(60, "slow"),
    MODERATE(80, "moderate"),
    NORMAL(100, "normal"),
    THRIVING(120, "thriving");

    private final int value;
    private final String name;

    GrowthSpeed(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int value() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static String allFormatted(String linebreaker) {
        StringBuilder formatted = new StringBuilder();
        Arrays.asList(GrowthSpeed.values()).forEach(x -> {
            formatted.append("- ").append(x.value()).append("% ").append(x.toString()).append(linebreaker);
        });
        return formatted.toString();
    }
}
