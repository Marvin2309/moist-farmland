package com.marvin23.moistfarmland.items;

import com.marvin23.moistfarmland.FarmlandStages;
import com.marvin23.moistfarmland.config.data.ConfigEntityHandler;
import com.marvin23.moistfarmland.config.operation.EntityValueByName;
import net.minecraft.block.*;
import net.minecraft.component.type.AttributeModifierSlot;
import net.minecraft.component.type.AttributeModifiersComponent;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.EntityAttributeModifier;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.WaterFluid;
import net.minecraft.item.*;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;

public final class WateringCanItem extends ToolItem {

    public WateringCanItem() {
        super(new WateringCanMaterial(), new Item.Settings().maxCount(1).attributeModifiers(createAttributeModifiers(new WateringCanMaterial(), 1, 1)));
    }

    @Override
    public ActionResult useOnBlock(ItemUsageContext context) {
        if(!context.getWorld().isClient) {
            PlayerEntity playerEntity = context.getPlayer();
            context.getWorld().getBlockState(context.getBlockPos()).getProperties();
            ItemStack stack = context.getStack();

            if (context.getWorld().getBlockState(context.getBlockPos()).getFluidState().getFluid() instanceof WaterFluid) {
                stack.setDamage(0);
                return ActionResult.SUCCESS;
            }

            int wateringCanDurability = new EntityValueByName<Integer>(ConfigEntityHandler.GENERAL_ENTITY_HANDLER, "timesTheWateringCanCanBeUsed").get().get();
            if(stack.getDamage() < wateringCanDurability-1) {
                context.getWorld().setBlockState(
                        context.getBlockPos(),
                        context.getWorld().getBlockState(context.getBlockPos()).with(FarmlandBlock.MOISTURE,
                        currentFarmlandStage(context.getWorld().getBlockState(context.getBlockPos())).upgradeByOne().value()),
                        Block.NOTIFY_LISTENERS);stack.damage(1, playerEntity,
                        LivingEntity.getSlotForHand(context.getHand()));
                playerEntity.sendMessage(Text.of(String.valueOf(stack.getDamage())));
                return ActionResult.SUCCESS;
            }
            return ActionResult.FAIL;
        }
        return ActionResult.FAIL;
    }

    private static AttributeModifiersComponent createAttributeModifiers(ToolMaterial material, float baseAttackDamage, float attackSpeed) {
        return AttributeModifiersComponent.builder().add(EntityAttributes.GENERIC_ATTACK_DAMAGE, new EntityAttributeModifier(ATTACK_DAMAGE_MODIFIER_ID, "Tool modifier", baseAttackDamage + material.getAttackDamage(), EntityAttributeModifier.Operation.ADD_VALUE), AttributeModifierSlot.MAINHAND).add(EntityAttributes.GENERIC_ATTACK_SPEED, new EntityAttributeModifier(ATTACK_SPEED_MODIFIER_ID, "Tool modifier", attackSpeed, EntityAttributeModifier.Operation.ADD_VALUE), AttributeModifierSlot.MAINHAND).build();
    }

    private FarmlandStages currentFarmlandStage(BlockState state) {
        int currentMoistureLevel = state.get(FarmlandBlock.MOISTURE);
        return FarmlandStages.fromInteger(currentMoistureLevel);
    }


}
