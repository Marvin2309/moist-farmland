package com.marvin23.moistfarmland.mixin;

import com.mojang.serialization.MapCodec;
import net.minecraft.block.*;
import net.minecraft.item.Items;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.property.IntProperty;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.World;
import net.minecraft.world.WorldView;
import org.spongepowered.asm.mixin.Mixin;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@Mixin(CropBlock.class)
public final class MixinCropBlock extends PlantBlock implements Fertilizable{

    public MixinCropBlock(Settings settings) {
        super(settings);
    }

    @Override
    public MapCodec<? extends PlantBlock> getCodec() {
        return CropBlock.createCodec(CropBlock::new);
    }

    @Override
    public boolean isFertilizable(WorldView world, BlockPos pos, BlockState state) {
        return false;
    }

    @Override
    public boolean canGrow(World world, Random random, BlockPos pos, BlockState state) {
        return true;
    }

    @Override
    public void grow(ServerWorld world, Random random, BlockPos pos, BlockState state) {
    }

    @Override
    public void randomTick(BlockState state, ServerWorld world, BlockPos pos, Random random) {
        CropBlock block = (CropBlock) state.getBlock();
        world.setBlockState(pos, state.with(growthAgePropertyOfBlock(block), state.get(growthAgePropertyOfBlock(block))+1), Block.NOTIFY_LISTENERS);
    }

    // Every day we stray further from god
    // This has to be done because some mods rely on their custom AgeProperty which is utilized by overwriting
    // the getAgeProperty in CropsBlock. Sadly this Method is protected that's why it is necessary for this instance
    // to make it accessible to allow ticking it.
    private <T extends CropBlock> IntProperty growthAgePropertyOfBlock(T cropBlock) {
        IntProperty ageProperty;
        try {
            Method method = cropBlock.getClass().getMethod("getAgeProperty");
            method.setAccessible(true);
            ageProperty = (IntProperty) method.invoke(cropBlock);
        } catch (NoSuchMethodException | InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        return ageProperty;
    }

}
