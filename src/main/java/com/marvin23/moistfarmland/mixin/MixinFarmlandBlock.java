package com.marvin23.moistfarmland.mixin;

import com.marvin23.moistfarmland.FarmlandStages;
import com.marvin23.moistfarmland.FarmlandUtil;
import com.marvin23.moistfarmland.ModRegistry;
import com.marvin23.moistfarmland.MoistFarmlandInitializer;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.FarmlandBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ElytraItem;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.tag.FluidTags;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.Hand;
import net.minecraft.util.ItemActionResult;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.random.Random;
import net.minecraft.world.World;
import net.minecraft.world.WorldView;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(FarmlandBlock.class)
public final class MixinFarmlandBlock extends Block {

	public MixinFarmlandBlock(Settings settings) {
		super(settings);
	}

	@Override
	public void randomTick(BlockState state, ServerWorld world, BlockPos pos, Random random) {
		if(FarmlandUtil.isWaterClose(world, pos) || world.isThundering()) {
			world.setBlockState(pos, state.with(FarmlandBlock.MOISTURE, FarmlandUtil.currentFarmlandStage(state).upgradeByOne().value()), Block.NOTIFY_LISTENERS);
			return;
		}

		if (world.hasRain(pos)) {
			world.setBlockState(pos, state.with(FarmlandBlock.MOISTURE, FarmlandUtil.currentFarmlandStage(state).upgradeByOne().value()), Block.NOTIFY_LISTENERS);

			return;
			}
		//TODO
		world.scheduleBlockTick(pos, this, 24000);
	}

	@Override
	public void scheduledTick(BlockState state, ServerWorld world, BlockPos pos, Random random) {

		if (FarmlandUtil.currentFarmlandStage(state).equals(FarmlandStages.PARCHED)) {
			FarmlandBlock.setToDirt(null, state, world, pos);
			return;
		}
		world.setBlockState(pos, state.with(FarmlandBlock.MOISTURE, FarmlandUtil.currentFarmlandStage(state).degradeByOne().value()), Block.NOTIFY_LISTENERS);
	}


}

