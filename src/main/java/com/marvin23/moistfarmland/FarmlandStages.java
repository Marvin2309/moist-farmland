package com.marvin23.moistfarmland;

import java.util.Arrays;

public enum FarmlandStages {
   PARCHED(0, 0), DRY(4, 1), MOIST(5,2), WET(6,3), WATERY(7, 4);

    private final int externalValue;
    private final int internalOrder;

    FarmlandStages(int externalValue, int internalOrder) {
        this.externalValue = externalValue;
        this.internalOrder = internalOrder;
    }

    public static FarmlandStages fromInteger(Integer number) {
        return Arrays.stream(FarmlandStages.values()).filter(stage -> number.equals(stage.externalValue)).findFirst().get();
    }

    public int value() {
        return externalValue;
    }

    public FarmlandStages degradeByOne() {
        if(isLowestStage(internalOrder)) {
            return FarmlandStages.PARCHED;
        }
        return fromInternalInteger(internalOrder-1);
    }

    public FarmlandStages upgradeByOne() {
        if(isHighestStage(internalOrder)) {
            return FarmlandStages.WATERY;
        }

        return fromInternalInteger(internalOrder+1);
    }
    private FarmlandStages fromInternalInteger(Integer internalNumber) {
        return Arrays.stream(FarmlandStages.values()).filter(stage -> internalNumber.equals(stage.internalOrder)).findFirst().get();
    }

    private boolean isHighestStage(int stage) {
        return 4 == stage;
    }

    private boolean isLowestStage(int stage) {
        return 0 == stage;
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
}
