package com.marvin23.moistfarmland;

import net.minecraft.block.BlockState;
import net.minecraft.block.FarmlandBlock;
import net.minecraft.registry.tag.FluidTags;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldView;

public final class FarmlandUtil {

    public static FarmlandStages currentFarmlandStage(BlockState state) {
        int currentMoistureLevel = state.get(FarmlandBlock.MOISTURE);
        return FarmlandStages.fromInteger(currentMoistureLevel);
    }

    public static boolean isWaterClose(WorldView world, BlockPos pos) {
        for (BlockPos blockPos : BlockPos.iterate(pos.add(-2, 0, -2), pos.add(2, 1, 2))) {
            if (!world.getFluidState(blockPos).isIn(FluidTags.WATER)) continue;
            return true;
        }
        return false;
    }
}
