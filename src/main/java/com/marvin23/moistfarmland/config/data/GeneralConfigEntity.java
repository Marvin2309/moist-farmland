package com.marvin23.moistfarmland.config.data;

import dev.isxander.yacl3.config.v2.api.SerialEntry;

public final class GeneralConfigEntity {

    @SerialEntry(comment = "24000 ticks is one minecraft day")
    public final int ticksNeededForUpdatingAFarmingTile = 24000;
    @SerialEntry
    public final int timesTheWateringCanCanBeUsed = 32;
}
