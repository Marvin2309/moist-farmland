package com.marvin23.moistfarmland.config.data;

import com.marvin23.moistfarmland.Constants;
import dev.isxander.yacl3.config.v2.api.ConfigClassHandler;
import dev.isxander.yacl3.config.v2.api.serializer.GsonConfigSerializerBuilder;
import dev.isxander.yacl3.platform.YACLPlatform;

public final class ConfigEntityHandler {
    public static ConfigClassHandler<FertilityConfigEntity> FERTILITY_ENTITY_HANDLER = ConfigClassHandler.createBuilder(FertilityConfigEntity.class)
            .serializer(config -> GsonConfigSerializerBuilder.create(config)
                    .setJson5(true)
                    .setPath(YACLPlatform.getConfigDir().resolve(Constants.MOD_NAME + "/fertility.json5"))
                    .build())
            .build();

    public static ConfigClassHandler<GeneralConfigEntity> GENERAL_ENTITY_HANDLER = ConfigClassHandler.createBuilder(GeneralConfigEntity.class)
            .serializer(config -> GsonConfigSerializerBuilder.create(config)
                    .setJson5(true)
                    .setPath(YACLPlatform.getConfigDir().resolve(Constants.MOD_NAME + "/general.json5"))
                    .build())
            .build();
}
