package com.marvin23.moistfarmland.config.data;

import com.marvin23.moistfarmland.FarmlandStages;
import com.marvin23.moistfarmland.GrowthSpeed;
import dev.isxander.yacl3.config.v2.api.SerialEntry;

import java.util.Map;
import java.util.TreeMap;

public final class FertilityConfigEntity {

    @SerialEntry(comment = "STOPPED(0%), VERY_SLOW(40%), SLOW(60%), MODERATE(80%), NORMAL(100%), THRIVING(120%)")
    public Map<String, Map<FarmlandStages, GrowthSpeed>> map = map();

    private Map<String, Map<FarmlandStages, GrowthSpeed>> map() {
        Map<String, Map<FarmlandStages, GrowthSpeed>> map = new TreeMap<>();

        Map<FarmlandStages, GrowthSpeed> wheat = new TreeMap<>();
        wheat.put(FarmlandStages.PARCHED, GrowthSpeed.STOPPED);
        wheat.put(FarmlandStages.DRY, GrowthSpeed.SLOW);
        wheat.put(FarmlandStages.MOIST, GrowthSpeed.NORMAL);
        wheat.put(FarmlandStages.WET, GrowthSpeed.NORMAL);
        wheat.put(FarmlandStages.WATERY, GrowthSpeed.SLOW);
        map.put("minecraft:wheat", wheat);

        Map<FarmlandStages, GrowthSpeed> carrots = new TreeMap<>();
        carrots.put(FarmlandStages.PARCHED, GrowthSpeed.STOPPED);
        carrots.put(FarmlandStages.DRY, GrowthSpeed.SLOW);
        carrots.put(FarmlandStages.MOIST, GrowthSpeed.NORMAL);
        carrots.put(FarmlandStages.WET, GrowthSpeed.SLOW);
        carrots.put(FarmlandStages.WATERY, GrowthSpeed.STOPPED);
        map.put("minecraft:carrots", carrots);

        return map;
    }
}
