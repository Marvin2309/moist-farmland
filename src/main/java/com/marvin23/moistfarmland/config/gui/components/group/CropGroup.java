package com.marvin23.moistfarmland.config.gui.components.group;

import com.marvin23.moistfarmland.FarmlandStages;
import com.marvin23.moistfarmland.GrowthSpeed;
import com.marvin23.moistfarmland.config.gui.components.FertileCropOptionEnum;
import com.marvin23.moistfarmland.config.operation.EntityValueByName;
import dev.isxander.yacl3.api.Option;
import dev.isxander.yacl3.api.OptionDescription;
import dev.isxander.yacl3.api.OptionGroup;
import net.minecraft.text.Text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public final class CropGroup implements Supplier<OptionGroup.Builder> {

    private final String cropName;
    private final String cropId;
    private final EntityValueByName<Map<String, Map<FarmlandStages, GrowthSpeed>>> valueByName;

    public CropGroup(String cropName, String cropId, EntityValueByName<Map<String, Map<FarmlandStages, GrowthSpeed>>> valueByName) {
        this.cropName = cropName;
        this.cropId = cropId;
        this.valueByName = valueByName;
    }
    @Override
    public OptionGroup.Builder get() {
        return OptionGroup.createBuilder()
                .name(Text.of(cropName))
                .description(OptionDescription.of(Text.of(cropName + " growth speed")))
                .collapsed(true)
                .options(options(valueByName.get().get().get(cropId)));
    }

    private static Collection<? extends Option<?>> options(Map<FarmlandStages, GrowthSpeed> speedByStage) {
        return speedByStage.keySet().stream()
                .map(currentFarmlandStage -> new FertileCropOptionEnum(
                        currentFarmlandStage,
                        () -> speedByStage.get(currentFarmlandStage),
                        newGrowthSpeed -> speedByStage.put(currentFarmlandStage, newGrowthSpeed))
                        .get().build())
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
