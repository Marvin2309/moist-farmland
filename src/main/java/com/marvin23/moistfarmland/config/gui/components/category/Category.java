package com.marvin23.moistfarmland.config.gui.components.category;

import dev.isxander.yacl3.api.ConfigCategory;
import net.minecraft.text.Text;

import java.util.function.Supplier;

public final class Category implements Supplier<ConfigCategory.Builder> {

    private final String name;
    private final String tooltip;

    public Category(String name, String tooltip) {
        this.name = name;
        this.tooltip = tooltip;
    }

    @Override
    public ConfigCategory.Builder get() {
            return ConfigCategory.createBuilder()
                    .name(Text.of(name))
                    .tooltip(Text.of(tooltip));
    }
}
