package com.marvin23.moistfarmland.config.gui.components.group;

import dev.isxander.yacl3.api.OptionDescription;
import dev.isxander.yacl3.api.OptionGroup;
import net.minecraft.text.Text;

import java.util.function.Supplier;


public final class Group implements Supplier<OptionGroup.Builder> {

    private final String name;

    public Group(String name) {
        this.name = name;
    }

    @Override
    public OptionGroup.Builder get() {
        return OptionGroup.createBuilder()
                .name(Text.of(name))
                .description(OptionDescription.of(Text.of(name + "grow speed")));
    }
}
