package com.marvin23.moistfarmland.config.gui.components;

import dev.isxander.yacl3.api.Binding;
import dev.isxander.yacl3.api.OptionDescription;
import dev.isxander.yacl3.gui.controllers.slider.IntegerSliderController;
import net.minecraft.text.Text;
import dev.isxander.yacl3.api.Option;

import java.util.function.Supplier;

public final class FertileCropOptionSlider implements Supplier<Option.Builder<Integer>> {

    private final String name;
    private final String description;
    private final Binding<Integer> binding;

    public FertileCropOptionSlider(String name, String description, Binding<Integer> binding) {
        this.name = name;
        this.description = description;
        this.binding = binding;
    }

    // TODO
    // Do I need this?
    @Override
    public Option.Builder<Integer> get() {
        Integer intOption = 1;
        return Option.<Integer>createBuilder()
                .name(Text.of(name))
                .description(OptionDescription.of(Text.of(description)))
                .binding(1, () -> intOption, newVal -> intOption.notify())
                .customController(option -> new IntegerSliderController(option, 0, 200, 5, val -> Text.of(val + "%"))
                );
    }
}
