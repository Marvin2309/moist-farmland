package com.marvin23.moistfarmland.config.gui.components;

import com.marvin23.moistfarmland.GrowthSpeed;

public final class TextConstants {
    public static final String fertileCropOptionEnumDescription = "How fast should this Crop grow on the specified soil. \n \n" + GrowthSpeed.allFormatted("\n");

}
