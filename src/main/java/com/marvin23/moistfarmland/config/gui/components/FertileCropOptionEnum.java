package com.marvin23.moistfarmland.config.gui.components;

import com.marvin23.moistfarmland.FarmlandStages;
import com.marvin23.moistfarmland.GrowthSpeed;
import dev.isxander.yacl3.api.Option;
import dev.isxander.yacl3.api.OptionDescription;
import dev.isxander.yacl3.impl.controller.EnumControllerBuilderImpl;
import net.minecraft.text.Text;

import java.util.function.Consumer;
import java.util.function.Supplier;


public final class FertileCropOptionEnum implements Supplier<Option.Builder<GrowthSpeed>>{

    private final FarmlandStages stages;
    private final Supplier<GrowthSpeed> getter;
    private final Consumer<GrowthSpeed> setter;

    public FertileCropOptionEnum(FarmlandStages stages, Supplier<GrowthSpeed> getter, Consumer<GrowthSpeed> setter) {
        this.stages = stages;
        this.getter = getter;
        this.setter = setter;
    }


    @Override
    public Option.Builder<GrowthSpeed> get() {
        return Option.<GrowthSpeed>createBuilder()
                        .name(Text.of(stages.toString() + " soil"))
                        .description(OptionDescription.of(Text.of(TextConstants.fertileCropOptionEnumDescription)))
                        .binding(getter.get(), getter, setter).customController(
                                option -> new EnumControllerBuilderImpl<>(option)
                                        .enumClass(GrowthSpeed.class)
                                        .build());
    }
}
