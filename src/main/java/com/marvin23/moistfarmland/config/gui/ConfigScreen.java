package com.marvin23.moistfarmland.config.gui;

import com.marvin23.moistfarmland.config.data.ConfigEntityHandler;
import com.marvin23.moistfarmland.config.gui.components.category.Category;
import com.marvin23.moistfarmland.config.gui.components.group.CropGroup;
import com.marvin23.moistfarmland.config.operation.EntityValueByName;
import dev.isxander.yacl3.api.*;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.text.Text;

import java.util.function.Supplier;



public final class ConfigScreen implements Supplier<Screen> {

    private final Screen parentScreen;

    public ConfigScreen(Screen parentScreen) {
        this.parentScreen = parentScreen;
    }

    @Override
    public Screen get() {
        YetAnotherConfigLib builder1 =
                YetAnotherConfigLib.createBuilder()
                .title(Text.of("Moist Farmland Config Screen"))
                    .category(new Category("Grow Speed Of Crops", "Config for Fertility for every soil").get()
                        .group(new CropGroup("carrots", "minecraft:carrots",
                                new EntityValueByName<>(
                                        ConfigEntityHandler.FERTILITY_ENTITY_HANDLER, "map"))
                                .get().build())
                            .group(new CropGroup("potato", "minecraft:potato",
                                    new EntityValueByName<>(
                                            ConfigEntityHandler.FERTILITY_ENTITY_HANDLER, "map"))
                                    .get().build()).build())
                        .save(() -> ConfigEntityHandler.FERTILITY_ENTITY_HANDLER.save()).build();

        return builder1.generateScreen(parentScreen);
    }
}
