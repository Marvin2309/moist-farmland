package com.marvin23.moistfarmland.config.operation;

import dev.isxander.yacl3.config.v2.api.ConfigClassHandler;
import dev.isxander.yacl3.config.v2.api.ConfigField;
import dev.isxander.yacl3.config.v2.api.FieldAccess;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.Supplier;

public final class EntityValueByName<T> implements Supplier<Optional<T>> {

    private final ConfigClassHandler<?> handler;
    private final String name;

    public EntityValueByName(ConfigClassHandler<?> handler, String name) {
        this.handler = handler;
        this.name = name;
    }

    @Override
    public Optional<T> get() {
        Optional<? extends FieldAccess<?>> access = Arrays.stream(handler.fields())
                .filter(field -> field.access().name().equals(name))
                .map(ConfigField::access)
                .findFirst();

        if (access.isPresent()) {
            FieldAccess<?> fieldAccess = access.get();
            T value = (T) fieldAccess.get();
            return Optional.ofNullable(value);
        } else {
            return Optional.empty();
        }
    }
}
