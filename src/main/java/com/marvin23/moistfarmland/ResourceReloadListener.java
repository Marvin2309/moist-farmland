package com.marvin23.moistfarmland;

import net.fabricmc.fabric.api.resource.SimpleSynchronousResourceReloadListener;
import net.minecraft.resource.Resource;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;

import java.io.InputStream;
import java.util.Map;

public final class ResourceReloadListener implements SimpleSynchronousResourceReloadListener {
    @Override
    public Identifier getFabricId() {
        return new Identifier(Constants.MOD_ID, Constants.MOD_ID);
    }

    @Override
    public void reload(ResourceManager manager) {
        Map<Identifier, Resource> identifiers = manager.findResources("resource-folder", path -> path.getPath().endsWith(".json"));

        for(Identifier id : identifiers.keySet()) {
            try(InputStream inputStream = manager.getResource(id).get().getInputStream()) {

                inputStream.close();
            } catch(Exception e) {
                ModRegistry.LOGGER.error("Error occurred while loading resource json " + id.toString(), e);

            }

        }
    }
}
