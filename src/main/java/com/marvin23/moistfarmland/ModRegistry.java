package com.marvin23.moistfarmland;

import com.marvin23.moistfarmland.items.WateringCanItem;
import net.minecraft.item.Item;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class ModRegistry {

    public static final Item WATERING_CAN = new WateringCanItem();
    public static final Logger LOGGER = LoggerFactory.getLogger(Constants.MOD_ID);
}
