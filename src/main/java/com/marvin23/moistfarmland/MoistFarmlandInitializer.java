package com.marvin23.moistfarmland;

import com.marvin23.moistfarmland.config.data.ConfigEntityHandler;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.minecraft.item.ItemGroups;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

public final class MoistFarmlandInitializer implements ModInitializer {

	@Override
	public void onInitialize() {

		ConfigEntityHandler.FERTILITY_ENTITY_HANDLER.load();
		ConfigEntityHandler.GENERAL_ENTITY_HANDLER.load();

		Registry.register(Registries.ITEM, new Identifier(Constants.MOD_ID, Constants.Items.WATERING_CAN), ModRegistry.WATERING_CAN);
		ItemGroupEvents.modifyEntriesEvent(ItemGroups.TOOLS).register(content -> {
			content.add(ModRegistry.WATERING_CAN);
		});

	}
}
