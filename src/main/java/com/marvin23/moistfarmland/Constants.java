package com.marvin23.moistfarmland;

public final class Constants {
    public static final String MOD_ID = "moistfarmland";
    public static final String MOD_NAME = "moist-farmland";

    public static class Properties {
        public static final String WATERING_CAN_FILLING = "watering_can_filling";
    }
    public static class Blocks {
    }
    public static class Items {
        public static final String WATERING_CAN = "watering_can";
    }
}